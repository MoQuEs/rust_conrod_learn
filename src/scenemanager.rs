#[macro_export]
macro_rules! scene {
    () => {
        #[derive(Debug, Clone)]
	    pub struct Scene {
			is_loaded: bool
		}
	};
    ({ $( $field:ident: $ty:ty ),* }) => {
        #[derive(Debug, Clone)]
	    pub struct Scene {
			is_loaded: bool,
			$( $field: $ty ),*
		}
	};
}


scene!();


pub struct SceneManager {
	curr_scene: Scene
}


pub trait SceneTrait {
	fn new() -> Self;
	fn load(&self) -> bool;
	fn is_loaded(&self) -> &bool;
	fn unload(&self) -> bool;
	fn update(&self);
	fn redraw(&self);
}


impl SceneManager {
	pub fn new(curr_scene: Scene) -> Self {
		SceneManager { curr_scene }
	}
	
	pub fn change_scene(&mut self, new_scene: Scene) {
		if !self.curr_scene.is_loaded() {
			self.curr_scene.unload();
		}
		self.curr_scene = new_scene;
	}
	
	pub fn load(&mut self) -> bool {
		self.curr_scene.load()
	}
	
	pub fn is_loaded(&self) -> bool {
		!!self.curr_scene.is_loaded()
	}
	
	pub fn unload(&mut self) -> bool {
		self.curr_scene.unload()
	}
	
	pub fn update(&mut self) {
		self.curr_scene.update();
	}
	
	pub fn redraw(&mut self) {
		self.curr_scene.redraw();
	}
}
