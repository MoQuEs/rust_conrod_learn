#[macro_use]
extern crate conrod;


mod eventloop;
mod scenemanager;
mod scenes;
mod gui;


use conrod::{UiBuilder, widget, Positionable, Widget, image, color, Theme, theme};
use conrod::position::{Align, Direction, Padding, Position, Relative};
use conrod::backend::glium::{Renderer};
use conrod::backend::glium::glium::{Surface, Display, texture};
use conrod::backend::glium::glium::glutin::{WindowBuilder, ContextBuilder,
                                            EventsLoop, Event, WindowEvent,
                                            KeyboardInput, VirtualKeyCode};

use eventloop::EventLoop;
use scenemanager::SceneManager;
use scenes::loading;
use gui::styles;

/* GRAPHICS OPTIONS--------------------------------------------------------------------
        Define window details here. If you have an .ini file where users can set preferences for
        graphics options, this would be the place they are read and stored. Place here graphics
        settings such as resolution, vsync on/off, multisampling level, etc. Contents of this
        section will be used in INITIALIZATION PROCEDURES. */

const WIDTH: u32 = 1024;
const HEIGHT: u32 = 768;


fn main() {
	/* INITIALIZATION PROCEDURES--------------------------------------------
        Following blocks of code build the necessary infrastructure for conrod's
        functioning.*/
	
	// Build the window.
	let mut events_loop = EventsLoop::new();
	let window = WindowBuilder::new()
		.with_title("Hello Conrod!")
		.with_dimensions(WIDTH, HEIGHT);
	let context = ContextBuilder::new()
		.with_vsync(true)
		.with_multisampling(4);
	let display = Display::new(window, context, &events_loop)
		.unwrap();
	
	// construct our `Ui`.
	let mut ui = UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();
	
	// Generate the widget identifiers.
	widget_ids!(struct Ids {text,text2});
	let ids = Ids::new(ui.widget_id_generator());
	
	let mut main_styles = styles::main_styles(&mut ui);
	
	// A type used for converting `conrod::render::Primitives` into `Command`s that can be used
	// for drawing to the glium `Surface`.
	let mut renderer = Renderer::new(&display).unwrap();
	
	// The image map describing each of our widget->image mappings (in our case, none).
	let image_map = image::Map::<texture::Texture2d>::new();
	
	let mut event_loop = EventLoop::new();
	'render: loop {
		/* RENDERING LOOP'S INFRASTRUCTURE--------------------------------------
            Following code is the plumbing which should be present in every render loop
            for conrod to function as intended. This section gets the events ready
            for processing. (Needs some sort of laconic explanation WHY the things here
            are the way they are.)*/
		
		// Handle all events.
		'event: for event in event_loop.next(&mut events_loop) {
			// Use the `winit` backend feature to convert the winit event to a conrod one.
			if let Some(event) = conrod::backend::winit::convert_event(event.clone(), &display) {
				ui.handle_event(event);
				event_loop.needs_update();
			}
			
			/* INPUT PROCESSING------------------------------------------------------
                Use this section of the code to process the input. Add new matches here
                for input as you develop functionality for them. This section's responsibility
                should be to receive the input events and prepare them for "ui" event
                handling. */
			
			println!("{:?}", &event);
			
			match event {
				Event::WindowEvent { event, .. } => match event {
					// Break from the loop upon `Escape`.
					WindowEvent::Closed |
					WindowEvent::KeyboardInput {
						input: KeyboardInput {
							virtual_keycode: Some(VirtualKeyCode::Escape),
							..
						},
						..
					} => break 'render,
					_ => (),
				},
				_ => (),
			}
			
			{
				// Set the widgets.
				let ui = &mut ui.set_widgets();
				
				/* BUILDING OF THE USER INTERFACE-----------------------------------
                This is the place where you write the code for the elements of your UI.
                This section should be responsible for all your widgets, buttons,
                primitives and text which will appear on the screen during the use. */
			}
		}
		
		{
			// Set the widgets.
			let ui = &mut ui.set_widgets();
			
			/* BUILDING OF THE USER INTERFACE-----------------------------------
			This is the place where you write the code for the elements of your UI.
			This section should be responsible for all your widgets, buttons,
			primitives and text which will appear on the screen during the use. */
			
			// "Hello World!" in the middle of the screen.
			widget::Text::new("bottom_right_of!")
				.with_style(*main_styles.get("main_title").unwrap())
				.middle_of(ui.window)
				.set(ids.text, ui);
		}
		
		// Render the `Ui` and then display it on the screen.
		if let Some(primitives) = ui.draw_if_changed() {
			renderer.fill(&display, primitives, &image_map);
			let mut target = display.draw();
			target.clear_color(0.0, 0.0, 0.0, 1.0);
			renderer.draw(&display, &mut target, &image_map).unwrap();
			target.finish().unwrap();
		}
	}
}
