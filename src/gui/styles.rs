use std::collections::HashMap;

use conrod::Ui;
use conrod::color::Color;
use conrod::widget::text::Style;


pub fn main_styles(ui: &mut Ui) -> HashMap<&'static str, Style> {
	let mut style: HashMap<&'static str, Style> = HashMap::new();
	
	// Add a `Font` to the `Ui`'s `font::Map` from file.
	let font_path: &'static str = concat!(env!("CARGO_MANIFEST_DIR"), "/assets/fonts");
	let font_noto = ui.fonts.insert_from_file(font_path.to_string() + "/NotoSans/NotoSans-Regular.ttf").unwrap();
	let font_vonique = ui.fonts.insert_from_file(font_path.to_string() + "/vonique_64/Vonique 64.ttf").unwrap();
	
	style.insert("main_title", Style {
		font_size: Some(32),
		color: Some(Color::Rgba(0.5, 0.5, 0.5, 0.5)),
		maybe_wrap: None,
		line_spacing: None,
		justify: None,
		font_id: Some(Some(font_vonique)),
	});
	
	style
}
